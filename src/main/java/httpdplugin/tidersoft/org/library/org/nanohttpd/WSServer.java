package httpdplugin.tidersoft.org.library.org.nanohttpd;




import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;


import httpdplugin.tidersoft.org.library.org.java_websocket.WebSocket;
import httpdplugin.tidersoft.org.library.org.java_websocket.framing.Framedata;
import httpdplugin.tidersoft.org.library.org.java_websocket.handshake.ClientHandshake;
import httpdplugin.tidersoft.org.library.org.java_websocket.server.WebSocketServer;

/**
 * A simple WebSocketServer implementation. Keeps track of a "chatroom".
 */
public class WSServer extends WebSocketServer {

	
	public WSServer( int port ) throws UnknownHostException {
		super( new InetSocketAddress( port ) );
		System.out.println("WebSocket Server start at port: "+port);
	}

	public WSServer( InetSocketAddress address ) {
		super( address );
		}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake ) {
		System.out.println( conn.getLocalSocketAddress().getAddress().getHostAddress() + " entered the room!" );
		//	conn.send("Hello!");
	}

	
	@Override
	public void onClose( WebSocket conn, int code, String reason, boolean remote ) {

		
		System.out.println( conn + " has left the room!" );
	}

        

      
      
	@Override
	public void onMessage( WebSocket conn, String message ) {

		JsonElement elem = new JsonParser().parse(message);
                JsonObject obj= elem.getAsJsonObject();
                String login = obj.get("login").getAsString();
                String pass = obj.get("pass").getAsString();

	}

	@Override
	public void onFragment( WebSocket conn, Framedata fragment ) {
		System.out.println( "received fragment: " + fragment );
	}
     
        

	@Override
	public void onError( WebSocket conn, Exception ex ) {
		ex.printStackTrace();
		if( conn != null ) {

		//	Engine.instance().CheckPlayers(connections());
			// some errors like port binding failed may not be assignable to a specific websocket
		}
	}

	
	
	/**
	 * Sends <var>text</var> to all currently connected WebSocket clients.
	 * 
	 * @param text
	 *            The String to send across the network.
	 * @throws InterruptedException
	 *             When socket related I/O errors occur.
	 */
	public void sendToAll( String text ) {
		Collection<WebSocket> con = connections();
		synchronized ( con ) {
			for( WebSocket c : con ) {
				c.send( text );
			}
		}
	}
        
       
}
